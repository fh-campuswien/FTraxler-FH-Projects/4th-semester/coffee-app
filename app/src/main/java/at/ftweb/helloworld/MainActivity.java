package at.ftweb.helloworld;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener()
    {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {
            switch (item.getItemId())
            {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.slogan_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //        mTextMessage = (TextView) findViewById(R.id.message);
        //        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        //        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void buttonClick_EventHandler(View view)
    {
        boolean cream = findViewById(R.id.cream_option).isSelected();
        boolean chocolate = findViewById(R.id.choco_option).isSelected();
        String name = ((EditText) findViewById(R.id.customer)).getText().toString();
        int quantity = Integer.parseInt(((TextView) findViewById(R.id.counterText)).getText().toString());
        SendMail(cream, chocolate, quantity, name);
    }

    private void SendMail(boolean cream, boolean chocolate, int quantity, String name)
    {
        String body = "Hi, I'd like to buy a coffee:\n" + "Whipped Cream: " + cream + "\n" + "Chocolate: " + chocolate + "\n" + "Quantity: " + quantity + "\n" + "Price: " + quantity * 3 + ".00 €\n" + "Kind Regards,\n" + name;

        Intent intent = new Intent(Intent.ACTION_SEND, Uri.fromParts("mailto", "order@getmya.coffee", null));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Coffee Order");
        intent.putExtra(Intent.EXTRA_TEXT, body);

        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    public void counterClickDown_Handler(View view)
    {
        TextView counterLabel = findViewById(R.id.counterText);
        if (counterLabel.getText() == "0")
            return;
        int value = Integer.parseInt(counterLabel.getText().toString());
        value--;
        counterLabel.setText(value);
    }

    public void counterClickUp_Handler(View view)
    {
        TextView counterLabel = findViewById(R.id.counterText);
        if (counterLabel.getText() == "10")
        {
            Toast.makeText(this, R.string.max_limit, Toast.LENGTH_LONG).show();
            return;
        }
        int value = Integer.parseInt(counterLabel.getText().toString());
        value++;
        counterLabel.setText(value);
    }
}
